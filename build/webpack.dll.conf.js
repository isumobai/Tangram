const path = require("path");
const webpack = require("webpack");

module.exports = {
  // 需要打包的模块
  entry: {
    vendor: ['vue/dist/vue.esm.js', 'vuex', 'vue-router', 'element-ui', 'lodash','jquery','axios']
  },
  output: {
    // 打包后文件输出的位置
    path: path.join(__dirname, '../static/js'),
    //打包文件的名字
    filename: '[name].dll.js',
    // [name].dll.js中暴露出的全局变量名
    // 主要是给webpack.DllPlugin()中的name使用，
    // 需要和webpack.DllPlugin中的`name: '[name]_library',`保持一致
    library: '[name]_library'
  },
  plugins: [
    new webpack.DllPlugin({
      //生成清单文件，在build文件夹下
      path: path.join(__dirname, '.', '[name]-manifest.json'),
      name: '[name]_library',
      context: __dirname
    }),
    // 压缩打包的文件
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,  //当删除没有用处的代码时，显示警告
        drop_console: true,  //干掉console.*函数
        drop_debugger:true,  // 移除 debugger
      },
      output:{
        // 去掉注释内容
        comments: false,
      },
      sourceMap: true
    })
  ]
};