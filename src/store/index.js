import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import _ from 'lodash'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 视图模式
    view: 'default',
    // 配置参数
    config: ''
  },
  mutations: {

    // 修改视图模式
    setView(state, val) {
      state.view = val;
    },

    // 修改config
    updatedConfig(state, parms) {
      _.set(state.config, parms.path, parms.value)
    },

    // 增加模块项
    addItem(state, path) {
      let item = _.get(state.config, path),
        length = item.length,
        lastItem = item[length - 1];

      let newItem = JSON.stringify(lastItem)
      item.push(JSON.parse(newItem))
    },

    // 删除项
    delItem(state, parms) {
      let item = _.get(state.config, parms.path);
      item.splice(parms.index, 1);
    },

    // 新增场景
    addScenes(state, parms){
      let item = _.get(state.config, parms.path);
      let newItem = JSON.stringify(parms.data);
      item.push(JSON.parse(newItem));
    },

    // 删除模块
    delModules(state,parms){
      let item = _.get(state.config, parms.path);
      item.splice(parms.index, 1);
    }
  },
  actions: {
    //获取配置JSON
    getConfig(context) {
      const url = 'static/config/base2.json';

      //获取场景配置
      axios.get(url)
        .then(res => {
          context.state.config = res.data;
        });
    }
  }
})
